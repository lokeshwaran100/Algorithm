#include "stdafx.h"
#include "Sorting.h"


void Sorting::PrintArray(int* array, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << array[i] << " ";
	}
	std::cout << std::endl;
}

void Sorting::DisplayBeforeSort(int* array, int size)
{
	std::cout << "Before: ";
	PrintArray(array, size);
}

void Sorting::DisplayAfterSort(int* array, int size)
{
	std::cout << "After:  ";
	PrintArray(array, size);
}

void Sorting::IterResult(TestArray& test_array)
{
	int size = 0;
	int* array = test_array.GenerateArray(size);
	std::cout << name << std::endl;
	DisplayBeforeSort(array, size);
	IterSort(array, size);
	DisplayAfterSort(array, size);
}

void Sorting::RecurResult(TestArray& test_array)
{
	int size = 0;
	int* array = test_array.GenerateArray(size);
	std::cout << name << std::endl;
	DisplayBeforeSort(array, size);
	RecurSort(array, size);
	DisplayAfterSort(array, size);
}

// TODO: Including binary search instead of linear search
// TODO: Use double linked list for insertion
void InsertionSort::IterSort(int* array, int size)
{
	int i;
	for (int pos = 1; pos < size; pos++)
	{
		int key = array[i = pos];

		while (i > 0 && key < array[i - 1])
		{
			array[i] = array[i - 1];
			i--;
		}

		array[i] = key;
	}
}
void InsertionSort::RecurSort(int* array, int size)
{
}


//TODO: implement iterative sort
void MergeSort::IterSort(int* array, int size)
{
}

void MergeSort::RecurSort(int* array, int size)
{
	_MergeSort(array, 0, size - 1);
}

void MergeSort::_MergeSort(int* array, int x, int z)
{
	if (x < z)
	{
		int y = (x + z) / 2;
		_MergeSort(array, x, y);
		_MergeSort(array, y + 1, z);
		Merge(array, x, y, z);
	}
}

void MergeSort::Merge(int* array, int p, int q, int r)
{
	int s1 = q - p + 1;
	int s2 = r - q;

	std::unique_ptr<int[]> a1(new int[s1 + 1]);
	std::unique_ptr<int[]> a2(new int[s2 + 1]);

	memcpy(&(a1[0]), (array + p), s1 * sizeof(int));
	memcpy(&(a2[0]), (array + q + 1), s2 * sizeof(int));

	a1[s1] = INT16_MAX;
	a2[s2] = INT16_MAX;

	for (int k = p, i = 0, j = 0; k <= r; k++)
	{
		if (a1[i] <= a2[j])
		{
			array[k] = a1[i];
			i++;
		}
		else
		{
			array[k] = a2[j];
			j++;
		}
	}
}


//TODO: implement iterative sort
void QuickSort::IterSort(int* array, int size)
{
}

void QuickSort::RecurSort(int* array, int size)
{
	_QuickSort(array, 0, size - 1);
}

void QuickSort::_QuickSort(int* array, int x, int z)
{
	if (x < z)
	{
		int y = Partition(array, x, z);
		_QuickSort(array, x, y - 1);
		_QuickSort(array, y + 1, z);
	}
}

int QuickSort::Partition(int* array, int p, int r)
{
	int last_elem = array[r];

	int i = p - 1, temp;

	for (int j = p; j <= r - 1; j++)
	{
		if (last_elem >= array[j])
		{
			i++;
			temp = array[j];
			array[j] = array[i];
			array[i] = temp;
		}
	}
	
	i++;

	temp = array[i];
	array[i] = array[r];
	array[r] = temp;

	return i;
}