#pragma once
#include <stdlib.h>
#include <iostream>
#include <string>
#include <memory>

class TestArray
{
	int size;
	std::unique_ptr<int[]> a;
public:
	TestArray(int size)
	{
		this->size = size;
		a = std::unique_ptr<int[]>(new int[this->size]);
	}

	int* GenerateArray(int& size)
	{
		size = this->size;
		for (int i = 0; i < size; i++)
		{
			a[i] = rand() % 10;
		}

		return a.get();
	}

	void GetLastArray(int* array, int& size)
	{
		size = this->size;
		for (int i = 0; i < size; i++)
		{
			array[i] = a[i];
		}
	}
};


class Sorting
{
private:
	void PrintArray(int* array, int size);
	void DisplayBeforeSort(int* array, int size);
	void DisplayAfterSort(int* array, int size);
protected:
	std::string name;
	virtual void IterSort(int* array, int size) = 0;
	virtual void RecurSort(int* array, int size) = 0;
public:
	virtual ~Sorting()
	{

	}
	void IterResult(TestArray& test_array);
	void RecurResult(TestArray& test_array);
};

class InsertionSort : public Sorting
{
private:
	void IterSort(int* array, int size);
	void RecurSort(int* array, int size);
public:
	InsertionSort()
	{
		name = "InsertionSort";
	}
};

class MergeSort : public Sorting
{
private:
	void IterSort(int* array, int size);
	void RecurSort(int* array, int size);
	void _MergeSort(int* array, int x, int y);
	void Merge(int* array, int p, int q, int r);
public:
	MergeSort()
	{
		name = "MergeSort";
	}
};

class QuickSort : public Sorting
{
private:
	void IterSort(int* array, int size);
	void RecurSort(int* array, int size);
	void _QuickSort(int* array, int x, int z);
	int Partition(int* array, int p, int r);
public:
	QuickSort()
	{
		name = "QuickSort";
	}
};