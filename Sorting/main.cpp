// Sorting.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Sorting.h"

#define SIZE 3

int main()
{
	TestArray array(SIZE);

	/*std::unique_ptr<Sorting> insertion_sort(new InsertionSort());
	insertion_sort->Result(array);*/

	/*std::unique_ptr<Sorting> merge_sort(new MergeSort());
	merge_sort->RecurResult(array);*/

	std::unique_ptr<Sorting> merge_sort(new QuickSort());
	merge_sort->RecurResult(array);

	getchar();

    return 0;
}

