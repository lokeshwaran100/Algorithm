#pragma once

#ifdef TestInput_EXPORTS
#define TESTINPUT_API __declspec(dllexport)   
#else  
#define TESTINPUT_API __declspec(dllimport)   
#endif  

#include <memory>

class TestInput
{
	int size;
	std::unique_ptr<int[]> a;
public:
	TESTINPUT_API TestInput(int size);
	TESTINPUT_API ~TestInput();
	TESTINPUT_API int* GenerateArray(int& size);
	TESTINPUT_API void GetLastArray(int* array, int& size);
};

