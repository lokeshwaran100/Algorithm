
#include "TestInput.h"


TestInput::TestInput(int size)
{
	this->size = size;
	a = std::unique_ptr<int[]>(new int[this->size + 20]);
}


TestInput::~TestInput()
{
}


int* TestInput::GenerateArray(int& size)
{
	size = this->size;
	for (int i = 0; i < size; i++)
	{
		a[i] = rand() % 10;
	}

	return a.get();
}

void TestInput::GetLastArray(int* array, int& size)
{
	size = this->size;
	for (int i = 0; i < size; i++)
	{
		array[i] = a[i];
	}
}