// Stack.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "TestInput.h"
#include "CStack.h"

#define STACK_SIZE 5

int main()
{
	TestInput input(STACK_SIZE);

	int size;
	int* array = input.GenerateArray(size);

	CStack stack(size);

	for (int i = 0; i <= size; i++)
	{
		std::cout << "push it " << i + 1 << std::endl;
		try
		{
			stack.push(array[i]);
			stack.display();
		}
		catch (std::exception exception)
		{
			std::cout << exception.what() << std::endl;
		}
	}

	getchar();

    return 0;
}

