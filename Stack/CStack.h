#pragma once

#include <memory>

class CStack
{
private:
	const uint16_t max_size = 0;
	std::unique_ptr<int[]> array;
	int index;
	CStack();
public:
	CStack(uint16_t size);
	~CStack();

	void push(int element);
	int pop();
	int size();
	bool isEmpty();
	void display();
};

