#include "stdafx.h"
#include "CStack.h"
#include <iostream>
#include <exception>

CStack::CStack()
{
}


CStack::CStack(const uint16_t max_size) : max_size(max_size)
{
	index = -1;
	array = std::unique_ptr<int[]>(new int[max_size]);
}

CStack::~CStack()
{
}

void CStack::push(int element)
{
	if (size() == max_size)
	{
		throw std::exception("Stack is full");
	}

	index++;
	array[index] = element;
}

int CStack::pop()
{
	if (isEmpty())
	{
		throw std::exception("Stack is empty");
	}

	int element = array[index];
	index--;

	return element;
}

int CStack::size()
{
	return index + 1;
}

bool CStack::isEmpty()
{
	return (size() == 0);
}

void CStack::display()
{
	int stack_size = size();
	for (int i = stack_size - 1; i >= 0; i--)
	{
		std::cout << "| " << array[i] << " |" << std::endl;
	}
}
