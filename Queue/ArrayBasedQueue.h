#pragma once

#include <memory>
#include "IQueue.h"

template <class Type>
class ArrayBasedQueue :
	public IQueue<Type>
{
private:
	uint16_t front;
	uint16_t rear;
	size_t max_size;
	std::unique_ptr<Type[]> array;
	ArrayBasedQueue();
public:
	ArrayBasedQueue(size_t max_size);
	ArrayBasedQueue(const ArrayBasedQueue& queue);
	ArrayBasedQueue(ArrayBasedQueue&& queue);
	~ArrayBasedQueue();
	void pushBack(Type element);
	Type popFront();
	size_t size();
	bool isEmpty();
	void display();
};

template <typename Type>
ArrayBasedQueue<Type>::ArrayBasedQueue()
{
	this->max_size = 0;
	front = rear = 0;
	array = nullptr;
}

template <typename Type>ArrayBasedQueue<Type>::ArrayBasedQueue(size_t max_size)
{
	this->max_size = max_size + 1;
	front = rear = 0;
	array = std::unique_ptr<Type[]>(new Type[this->max_size]);
	std::cout << "ArrayBaseQueue(array_size=" << max_size << ")" << std::endl;
}

template<typename Type>
ArrayBasedQueue<Type>::ArrayBasedQueue(const ArrayBasedQueue& queue)
{
	this->max_size = queue.max_size;
	this->front = queue.front;
	this->rear = queue.rear;
	this->array = std::unique_ptr<Type[]>(new Type[queue.max_size]);
	std::copy(this->array.get(), queue.array.get(), this->max_size);
}

template<typename Type>
ArrayBasedQueue<Type>::ArrayBasedQueue(ArrayBasedQueue&& queue)
{
	this->max_size = queue.max_size;
	this->front = queue.front;
	this->rear = queue.rear;
	this->array = std::move(queue.array);
}

template <typename Type>
ArrayBasedQueue<Type>::~ArrayBasedQueue()
{
}

template <typename Type>
void ArrayBasedQueue<Type>::pushBack(Type element)
{
	if (size() == max_size - 1)
	{
		throw std::exception("Queue is full");
	}

	array[rear] = element;
	rear = (rear + 1) % max_size;
}

template <typename Type>
Type ArrayBasedQueue<Type>::popFront()
{
	if (isEmpty())
	{
		throw std::exception("Queue is empty");
	}

	Type element = array[front];
	front = (front + 1) % max_size;

	return element;
}

template <typename Type>
size_t ArrayBasedQueue<Type>::size()
{
	return (max_size - front + rear) % max_size;
}

template <typename Type>
bool ArrayBasedQueue<Type>::isEmpty()
{
	return (size() == 0);
}

template <typename Type>
void ArrayBasedQueue<Type>::display()
{
	size_t cur_size = size();
	std::cout << "size: " << cur_size << " => ";
	for (int i = 0; i < cur_size; i++)
	{
		std::cout << " | " << array[(front + i) % max_size];
	}
	std::cout << std::endl;
}
