#pragma once
#include <stdint.h>

template <class Type>
class IQueue
{
public:
	virtual void pushBack(Type element) = 0;
	virtual Type popFront() = 0;
	virtual size_t size() = 0;
	virtual bool isEmpty() = 0;
	virtual void display() = 0;
};