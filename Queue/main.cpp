// Queue.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "TestInput.h"
#include "ArrayBasedQueue.h"


#define QUEUE_SIZE 5

int main()
{
	TestInput input(QUEUE_SIZE);

	int size;
	int* array = input.GenerateArray(size);

	std::unique_ptr<IQueue<int>> queue(new ArrayBasedQueue<int>((unsigned int)QUEUE_SIZE));
	
	for (int i = 0; i <= size; i++)
	{
		std::cout << "push iteration: " << i + 1 << std::endl;
		try
		{
			queue->pushBack(array[i]);
			queue->display();
		}
		catch (std::exception exception)
		{
			std::cout << exception.what() << std::endl;
		}
	}

	for (int i = 0; i <= size; i++)
	{
		std::cout << "pop iteration: " << i + 1 << std::endl;
		try
		{
			std::cout << "popped: " << queue->popFront() << std::endl;
			queue->display();
		}
		catch (std::exception exception)
		{
			std::cout << exception.what() << std::endl;
		}
	}

	for (int i = 0; i <= size; i++)
	{
		std::cout << "push & pop iteration: " << i + 1 << std::endl;
		try
		{
			queue->pushBack(44);
			queue->display();
			std::cout << "popped: " << queue->popFront() << std::endl;
			queue->display();
		}
		catch (std::exception exception)
		{
			std::cout << exception.what() << std::endl;
		}
	}

	getchar();
	
    return 0;
}

