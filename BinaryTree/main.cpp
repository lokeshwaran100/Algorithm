// BinaryTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BinaryTree.h"

#define SIZE 8

int main()
{
	TestInput test_array(SIZE);

	BinaryTree binary_tree;

	int* array = binary_tree.Heapify(test_array);

	int size = SIZE;

	while (size > 1)
	{
		std::cout << binary_tree.ExtractMax(array, size) << std::endl;

		binary_tree.PrintArray(array, size);
	}

	size = SIZE;

	TestInput test_array_1(size);

	array = binary_tree.Heapify(test_array_1);

	binary_tree.InsertElement(array, size, size - 1, 9);

	binary_tree.PrintArray(array, size);

	binary_tree.addElement(array, size, 8);

	binary_tree.PrintArray(array, size);

	getchar();

    return 0;
}

