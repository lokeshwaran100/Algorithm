#pragma once
#include <stdlib.h>
#include <iostream>
#include <string>
#include <memory>

#include "TestInput.h"

class __declspec(dllimport) TestInput;

class BinaryTree
{
private:
	void DisplayBeforeSort(int* array, int size);
	void DisplayAfterSort(int* array, int size);
	void maxHeapify(int* array, int size, int index);
public:
	BinaryTree();
	~BinaryTree();
	int* Heapify(TestInput& array);
	void PrintArray(int* array, int size);
	int ExtractMax(int* array, int& size);
	void InsertElement(int* array, int& size, int index, int value);
	void addElement(int* array, int& size, int value);
};

