#include "stdafx.h"
#include "BinaryTree.h"


BinaryTree::BinaryTree()
{
}


BinaryTree::~BinaryTree()
{
}

int* BinaryTree::Heapify(TestInput & test_array)
{
	int size;
	int* array = test_array.GenerateArray(size);

	DisplayBeforeSort(array, size);

	for (int node = (size / 2)  + 1 - 1; node >= 1; node--)
	{
		maxHeapify(array, size, node);
	}

	DisplayAfterSort(array, size);

	return array;
}


void BinaryTree::maxHeapify(int* array, int size, int node)
{
	int left_node = 2 * (node);
	int right_node = 2 * (node) + 1;

	int largest = node;

	if (left_node <= size && array[left_node - 1] > array[node - 1])
	{
		largest = left_node;
	}

	if (right_node <= size && array[right_node - 1] > array[largest - 1])
	{
		largest = right_node;
	}

	if (largest != node)
	{
		int temp = array[largest - 1];
		array[largest - 1] = array[node - 1];
		array[node - 1] = temp;
		maxHeapify(array, size, largest);
	}
}

void BinaryTree::PrintArray(int* array, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << array[i] << " ";
	}
	std::cout << std::endl;
}

int BinaryTree::ExtractMax(int * array, int& size)
{
	if (size <= 0)
	{
		return -1;
	}

	int max = array[0];

	--size;

	array[0] = array[size];

	maxHeapify(array, size, 1);

	return max;
}

void BinaryTree::InsertElement(int * array, int & size, int index, int value)
{
	if (index >= size)
	{
		return;
	}

	array[index] = value;

	while ((index + 1) > 1 && array[index] > array[((index + 1) / 2 - 1)])
	{
		int temp = array[((index + 1) / 2 - 1)];
		array[((index + 1) / 2 - 1)] = array[index];
		array[index] = temp;
		index = (index + 1) / 2 - 1;
	}
}

void BinaryTree::addElement(int * array, int & size, int value)
{
	int index = size++;

	array[index] = value;	

	while (index + 1 > 1 && array[index] > array[(index + 1) / 2 - 1])
	{
		int temp = array[index];
		array[index] = array[(index + 1) / 2 - 1];
		array[(index + 1) / 2 - 1] = temp;
		index = (index + 1) / 2 - 1;
	}
}


void BinaryTree::DisplayBeforeSort(int* array, int size)
{
	std::cout << "Before: ";
	PrintArray(array, size);
}

void BinaryTree::DisplayAfterSort(int* array, int size)
{
	std::cout << "After:  ";
	PrintArray(array, size);
}